# Packages

all of the packages and software you can install with Poss

## Fusion Games X CLI

Fusion Games X is a game website that revolutionize gaming on the web

### Requirements

- Latest version of [Python](https://python.org)
- windows and linux are supported

### Commands

#### "install fusiongamesxcli"

this command installs the program

#### "run fusiongamesxcli"

this command runs the program

## Pycalculate

Pycalculate is a Python calculator. Pretty simple one.

### Requirements

- Latest version of [Python](https://python.org)
- windows and linux are supported

### Commands

#### "install pycalculate"

this command installs Pycalculate

#### "run pycalculate"

this command runs Pycalculate

## Git Python

Git Python can clone git repos from any remote repo.

### Requirements

- Latest version of [Python](https://python.org)
- Latest version of [Git](https://git-scm.com/)
- windows and linux are supported

### Commands

#### "install git-python"

this command installs git-python

#### "run git-python"

this command runs git-python