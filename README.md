# Poss Package Manager


Poss. Python, Open source, software. Poss is a open source software manager

## Getting started

### Requirements

- Latest version of [Python](https://python.org)
- Latest version of [Git](https://git-scm.com/)

### Installer

if you are using [CmdOS](https://gitlab.com/cmdos10/cmdos), this is pre-installed

for Poss i made a installer. it is in the installer branch

run these commands in the order that i give you :)

1st:
```
git clone -b installer https://gitlab.com/poss4/poss-package-manager
```
2ed:
```
cd poss-package-manager
```
3rd:
```
python3 Possinstaller.py
```
4th:
```
cd poss-package-manager
```
last:
```
python3 main.py
```
Now you have the Poss package manager installed

### Packages


## How to make your own package

to make your own package, go to HOWTOAKEPOSSCONFIG.md in the .pconfig folder